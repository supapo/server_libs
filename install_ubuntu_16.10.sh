#!/bin/bash

. install_common.sh
apt update
apt upgrade -y
apt install -y aptitude openssh-server vim nano libevent-2.0-5 libevent-dev libkrb5-dev libnuma-dev libjemalloc1 \
  g++ gcc libdouble-conversion-dev libmysql++-dev autoconf autoconf-archive bison build-essential cmake curl flex \
  git gperf libcap-dev libgoogle-glog-dev libsasl2-dev libssl-dev pkg-config sudo unzip wget libtool libboost-all-dev \
  scons libsnappy-dev bash-completion man make zip libnet-ifconfig-wrapper-perl libgtest-dev
  
  install_common
  ubuntu_shape
  echo "done";