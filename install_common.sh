#!/bin/bash

function install_common
{
	echo "installing libs!!!"
	cd deps
	chmod +x bin/*
	cp -a include/* /usr/local/include/
	cp -a lib/* /usr/local/lib/
	cp -a bin/* /usr/local/bin/
	cp -a certs /certs
	ldconfig
	cd -
}
function ubuntu_shape
{
	sed -i "s/auto_ptr/unique_ptr/g" /usr/include/mysql++/refcounted.h
	sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
	sed -i "s/\/bin\/pidof -o %PPID -x/\/usr\/bin\/pgrep --full/g" /lib/lsb/init-functions
	sed -i "s/#force_color_prompt=yes/force_color_prompt=yes/g" /root/.bashrc
	echo "alias ..='cd ..'" >> /root/.bashrc
}